package com.example.bookingcoworkingspace;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentTransaction;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import com.example.bookingcoworkingspace.Model.BookingModel;

import java.util.ArrayList;

public class BookingAdapter extends RecyclerView.Adapter<BookingAdapter.MyViewHolder> {
    Context context;
    ArrayList<BookingModel> bookingArrayList;

    public BookingAdapter(Context context, ArrayList<BookingModel> bookingArrayList) {
        this.context = context;
        this.bookingArrayList = bookingArrayList;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(context).inflate(R.layout.list_item, parent, false);

        return new MyViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        BookingModel booking = bookingArrayList.get(position);

        holder.tvKodeBooking.setText(booking.kodeBooking);
        holder.tvFloorSectorDesk.setText(booking.floorSectorDesk);
        holder.tvDateTime.setText(booking.dateTime);
        holder.tvStatusBooking.setText(booking.statusBooking);

//        disable, not needed
//        holder.itemView.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                AppCompatActivity activity = (AppCompatActivity) view.getContext();
//                Fragment bookingDetailFragment = new BookingDetailFragment();
//                activity.getSupportFragmentManager().beginTransaction().replace(R.id.container_history, bookingDetailFragment).addToBackStack(null).commit();
//            }
//        });
    }

    @Override
    public int getItemCount() {
        return bookingArrayList == null ? 0 : bookingArrayList.size();
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {
        TextView tvKodeBooking, tvFloorSectorDesk, tvDateTime, tvStatusBooking;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            tvKodeBooking = itemView.findViewById(R.id.tvKodeBooking);
            tvFloorSectorDesk = itemView.findViewById(R.id.tvFloorSectorDesk);
            tvDateTime = itemView.findViewById(R.id.tvDateTime);
            tvStatusBooking = itemView.findViewById(R.id.tvStatusBooking);
        }
    }
}
