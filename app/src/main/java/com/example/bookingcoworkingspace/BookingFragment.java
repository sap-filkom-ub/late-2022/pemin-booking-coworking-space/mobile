package com.example.bookingcoworkingspace;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.TimePicker;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import static androidx.constraintlayout.helper.widget.MotionEffect.TAG;

import com.example.bookingcoworkingspace.SharedPreference.TimeBookingPreferences;

public class BookingFragment extends Fragment {
    RelativeLayout layout_date, layout_startTime, layout_endTime;
    TextView tv_date, tv_startTime, tv_endTime;
    DatePickerDialog.OnDateSetListener dateSetListener;
    TimePickerDialog timePickerDialogStart, timePickerDialogEnd;
    SimpleDateFormat timeFormatterStart, timeFormatterEnd;
    Button btnConfirm;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_booking, container, false);

        layout_date = view.findViewById(R.id.layout_date);
        layout_startTime = view.findViewById(R.id.layout_startTime);
        layout_endTime = view.findViewById(R.id.layout_endTime);
        tv_date = view.findViewById(R.id.tv_date);
        tv_startTime = view.findViewById(R.id.tv_startTime);
        tv_endTime = view.findViewById(R.id.tv_endTime);
        btnConfirm = view.findViewById(R.id.btnConfirm);


        /* Awal Date */
        layout_date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Calendar calendar = Calendar.getInstance();
                int year = calendar.get(Calendar.YEAR);
                int month = calendar.get(Calendar.MONTH);
                int day = calendar.get(Calendar.DAY_OF_MONTH);

                DatePickerDialog dialog = new DatePickerDialog(getContext(), android.R.style.Theme_DeviceDefault_Dialog, dateSetListener, year, month, day);
                dialog.show();
            }
        });
        dateSetListener = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker datePicker, int year, int month, int day) {
                Calendar calendar = Calendar.getInstance();
                calendar.set(year, month, day);

                SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
                String date = format.format(calendar.getTime());

                tv_date.setText(date);
            }
        };
        /* Akhir Date */

        /* Awal StartTime */
        layout_startTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                timeFormatterStart = new SimpleDateFormat("HH:mm", Locale.US);
                timePickerDialogStart.show();
            }
        });
        timePickerDialogStart = new TimePickerDialog(getActivity(), new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                Calendar c = Calendar.getInstance();
                c.set(Calendar.HOUR_OF_DAY, hourOfDay);
                c.set(Calendar.MINUTE, minute);
                tv_startTime.setText(new SimpleDateFormat("HH:mm", Locale.US).format(c.getTime()));
            }
        }, Calendar.getInstance().get(Calendar.HOUR_OF_DAY), Calendar.getInstance().get(Calendar.MINUTE), true);
        /* Akhir StartTime */

        /* Awal EndTime */
        layout_endTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                timeFormatterEnd = new SimpleDateFormat("HH:mm", Locale.US);
                timePickerDialogEnd.show();
            }
        });
        timePickerDialogEnd = new TimePickerDialog(getActivity(), new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                Calendar c = Calendar.getInstance();
                c.set(Calendar.HOUR_OF_DAY, hourOfDay);
                c.set(Calendar.MINUTE, minute);
                tv_endTime.setText(new SimpleDateFormat("HH:mm", Locale.US).format(c.getTime()));
            }
        }, Calendar.getInstance().get(Calendar.HOUR_OF_DAY), Calendar.getInstance().get(Calendar.MINUTE), true);
        /* Akhir EndTime */

        btnConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                TimeBookingPreferences.setDateSelected(getContext(), tv_date.getText().toString());
                TimeBookingPreferences.setStartTime(getContext(), tv_startTime.getText().toString());
                TimeBookingPreferences.setEndTime(getContext(), tv_endTime.getText().toString());

                Fragment orderFragment = new OrderFragment();
                FragmentTransaction fm = getActivity().getSupportFragmentManager().beginTransaction();
                fm.replace(R.id.flFragment, orderFragment, null).addToBackStack(null).commit();
            }
        });

        return view;
    }
}