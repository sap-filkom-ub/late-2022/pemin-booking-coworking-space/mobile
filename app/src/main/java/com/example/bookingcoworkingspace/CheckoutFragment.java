package com.example.bookingcoworkingspace;

import static android.content.ContentValues.TAG;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import com.example.bookingcoworkingspace.Interface.Desk;
import com.example.bookingcoworkingspace.Model.PostBookingRequest;
import com.example.bookingcoworkingspace.Model.PostBookingResponse;
import com.example.bookingcoworkingspace.SharedPreference.DataBookingPreferences;
import com.example.bookingcoworkingspace.SharedPreference.TimeBookingPreferences;
import com.example.bookingcoworkingspace.SharedPreference.UserPreferences;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class CheckoutFragment extends Fragment {
    TextView tvDate, tvSTime, tvETime, tvFloor, tvSector, tvDesk;
    Button btnConfirm;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_checkout, container, false);

        tvDate = view.findViewById(R.id.co_date);
        tvSTime = view.findViewById(R.id.co_sTime);
        tvETime = view.findViewById(R.id.co_eTime);
        tvFloor = view.findViewById(R.id.co_floor);
        tvSector = view.findViewById(R.id.co_sector);
        tvDesk = view.findViewById(R.id.co_desk);
        btnConfirm = view.findViewById(R.id.btnConfirm);

        tvDate.setText(TimeBookingPreferences.getDateSelected(getContext()));
        tvSTime.setText(TimeBookingPreferences.getStartTime(getContext()));
        tvETime.setText(TimeBookingPreferences.getEndTime(getContext()));
        tvFloor.setText(DataBookingPreferences.getFloorName(getContext()));
        tvSector.setText(DataBookingPreferences.getSectorName(getContext()));
        tvDesk.setText(DataBookingPreferences.getDeskName(getContext()));

        btnConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Gson gson = new GsonBuilder()
                        .setLenient()
                        .create();

                Retrofit retrofit = new Retrofit.Builder()
                        .baseUrl("https://coworking.sparc.id/api/")
                        .addConverterFactory(GsonConverterFactory.create())
                        .build();

                Desk desk = retrofit.create(Desk.class);

                PostBookingRequest bookingRequest = new PostBookingRequest(
                        UserPreferences.getUserId(getContext()),
                        DataBookingPreferences.getIdDesk(getContext()),
                        TimeBookingPreferences.getDateSelected(getContext()),
                        TimeBookingPreferences.getStartTime(getContext()),
                        TimeBookingPreferences.getEndTime(getContext())
                );

                Log.d(TAG, "onClick: bookingReq: \n" +
                        "UserToken: " + UserPreferences.getUserToken(getContext()) + "\n" +
                        "UserId: " + bookingRequest.getUser_id() + "\n" +
                        "DeskId: " + bookingRequest.getDesk_id() + "\n" +
                        "Date: " + bookingRequest.getDate() + "\n" +
                        "StartT: " + bookingRequest.getStart_time() + "\n" +
                        "EndT: " + bookingRequest.getEnd_time() + "\n");

                Call<PostBookingResponse> call = desk.postBooking(
                        UserPreferences.getUserToken(getContext()),
                        bookingRequest.getUser_id(),
                        bookingRequest.getDesk_id(),
                        bookingRequest.getDate(),
                        bookingRequest.getStart_time(),
                        bookingRequest.getEnd_time()
                );

                call.enqueue(new Callback<PostBookingResponse>() {
                    @Override
                    public void onResponse(Call<PostBookingResponse> call, Response<PostBookingResponse> response) {
                        if (!response.isSuccessful()) {
                            Toast.makeText(getContext(), "Error Code: " + response.code() + response.message(), Toast.LENGTH_LONG).show();
                        }
                        Log.d(TAG, "onResponse: \n" +
                                "Data Stored: \n" +
                                "UserId: " + response.body().getData().getUser_id() + "\n" +
                                "DeskId: " + response.body().getData().getDesk_id() + "\n" +
                                "Date: " + response.body().getData().getDate() + "\n" +
                                "Start Time: " + response.body().getData().getStart_time() + "\n" +
                                "End Time: " + response.body().getData().getEnd_time() + "\n\n");
                        Fragment historyBookingFragment = new HistoryBookingFragment();
                        FragmentTransaction fm = getActivity().getSupportFragmentManager().beginTransaction();
                        fm.replace(R.id.flFragment, historyBookingFragment, null).addToBackStack(null).commit();
                    }

                    @Override
                    public void onFailure(Call<PostBookingResponse> call, Throwable t) {
                        Toast.makeText(getContext(), "Error Code: " + t.getMessage(), Toast.LENGTH_LONG).show();
                    }
                });


            }
        });

        return view;
    }
}
