package com.example.bookingcoworkingspace;

import static android.content.ContentValues.TAG;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.bookingcoworkingspace.Interface.Desk;
import com.example.bookingcoworkingspace.Interface.Sector;
import com.example.bookingcoworkingspace.Model.DataDeskResponse;
import com.example.bookingcoworkingspace.Model.DeskRequest;
import com.example.bookingcoworkingspace.Model.DeskResponse;
import com.example.bookingcoworkingspace.Model.SectorResponse;
import com.example.bookingcoworkingspace.SharedPreference.DataBookingPreferences;
import com.example.bookingcoworkingspace.SharedPreference.TimeBookingPreferences;
import com.example.bookingcoworkingspace.SharedPreference.UserPreferences;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class DeskFragment extends Fragment {
    LinearLayout desk1, desk2, desk3;
    TextView tvDesk1, tvDesk2, tvDesk3, tvDesc1, tvDesc2, tvDesc3;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_desk, container, false);
        desk1 = view.findViewById(R.id.layout_desk1);
        desk2 = view.findViewById(R.id.layout_desk2);
        desk3 = view.findViewById(R.id.layout_desk3);
        tvDesk1 = view.findViewById(R.id.desk1);
        tvDesk2 = view.findViewById(R.id.desk2);
        tvDesk3 = view.findViewById(R.id.desk3);
        tvDesc1 = view.findViewById(R.id.desc1);
        tvDesc2 = view.findViewById(R.id.desc2);
        tvDesc3 = view.findViewById(R.id.desc3);

        String idDesk = DataBookingPreferences.getIdSector(getContext());
        String BASE_URL = "https://coworking.sparc.id/api/desk/" + idDesk + "/";

        Log.d(TAG, "\n onResponse: SectorIdSelected: " + DataBookingPreferences.getIdSector(getContext()) + "\n");
        Log.d(TAG, "\n onResponse: BASE_URL: " + BASE_URL + "\n");

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        Desk desk = retrofit.create(Desk.class);
        DeskRequest deskRequest = new DeskRequest(
                TimeBookingPreferences.getDateSelected(getContext()),
                TimeBookingPreferences.getStartTime(getContext()),
                TimeBookingPreferences.getEndTime(getContext())
        );
        Call<DeskResponse> call = desk.getDesk(UserPreferences.getUserToken(getContext()),
                deskRequest.getDate(),
                deskRequest.getStart_time(),
                deskRequest.getEnd_time());

        Log.d(TAG, "onCreateView: Desk URL: " + call + "\n" + deskRequest.getDate() + "\n" +
                deskRequest.getStart_time() + "\n" +
                deskRequest.getEnd_time());

        call.enqueue(new Callback<DeskResponse>() {
            @Override
            public void onResponse(Call<DeskResponse> call, Response<DeskResponse> response) {
                if (!response.isSuccessful()) {
                    Toast.makeText(getContext(), "Error Code: " + response.code() + "\nCannot Fetch Data", Toast.LENGTH_LONG).show();
                }
                Log.d(TAG, "onResponse: Receive Information: " +response.body().toString());

                ArrayList<DataDeskResponse> deskList = response.body().getData();

                for (int i = 0; i < deskList.size(); i++) {
                    Log.d(TAG, "onResponse: \n" +
                            "id: " + deskList.get(i).getId() + "\n" +
                            "Name: " + deskList.get(i).getName() + "\n" +
                            "Description: " + deskList.get(i).getDescription() + "\n\n");
                }

                tvDesk1.setText(deskList.get(0).getName());
                tvDesc1.setText(deskList.get(0).getDescription());
                tvDesk2.setText(deskList.get(1).getName());
                tvDesc2.setText(deskList.get(1).getDescription());
                tvDesk3.setText(deskList.get(2).getName());
                tvDesc3.setText(deskList.get(2).getDescription());

                desk1.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        DataBookingPreferences.setIdDesk(getContext(), deskList.get(0).getId());
                        DataBookingPreferences.setDeskName(getContext(), deskList.get(0).getName());
                        Fragment checkoutFragment = new CheckoutFragment();
                        FragmentTransaction fm = getActivity().getSupportFragmentManager().beginTransaction();
                        fm.replace(R.id.flFragment, checkoutFragment, null).addToBackStack(null).commit();
                    }
                });

                desk2.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        DataBookingPreferences.setIdDesk(getContext(), deskList.get(1).getId());
                        DataBookingPreferences.setDeskName(getContext(), deskList.get(1).getName());
                        Fragment checkoutFragment = new CheckoutFragment();
                        FragmentTransaction fm = getActivity().getSupportFragmentManager().beginTransaction();
                        fm.replace(R.id.flFragment, checkoutFragment, null).addToBackStack(null).commit();
                    }
                });

                desk3.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        DataBookingPreferences.setIdDesk(getContext(), deskList.get(2).getId());
                        DataBookingPreferences.setDeskName(getContext(), deskList.get(2).getName());
                        Fragment checkoutFragment = new CheckoutFragment();
                        FragmentTransaction fm = getActivity().getSupportFragmentManager().beginTransaction();
                        fm.replace(R.id.flFragment, checkoutFragment, null).addToBackStack(null).commit();
                    }
                });
            }

            @Override
            public void onFailure(Call<DeskResponse> call, Throwable t) {

            }
        });

        return view;
    }
}