package com.example.bookingcoworkingspace;

import static androidx.constraintlayout.helper.widget.MotionEffect.TAG;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.bookingcoworkingspace.Interface.Booking;
import com.example.bookingcoworkingspace.Model.BookingModel;
import com.example.bookingcoworkingspace.Model.DataGetBookingResponse;
import com.example.bookingcoworkingspace.Model.GetBookingResponse;
import com.example.bookingcoworkingspace.Model.PostBookingResponse;
import com.example.bookingcoworkingspace.SharedPreference.UserPreferences;

import java.util.ArrayList;
import java.util.Arrays;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class HistoryBookingFragment extends Fragment {
//    TextView kodeBooking, tvFloorSectorDesk, tvDateTime;
//    ListView listView;

    private ArrayList<BookingModel> bookingArrayList;
    //    private String[] kode_booking, floorSectorDesk, dateTime, statusBooking;
    private RecyclerView recyclerview;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_history_booking, container, false);

        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

//        !! Become like this because can't pass data from retrofit, it deny any access outside of onResponse
        bookingArrayList = new ArrayList<>();
        String userId = UserPreferences.getUserId(getContext());
        String BASE_URL = "https://coworking.sparc.id/api/booking/" + userId + "/";

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        Booking booking = retrofit.create(Booking.class);

        Call<GetBookingResponse> call = booking.getBookings(UserPreferences.getUserToken(getContext()));

        call.enqueue(new Callback<GetBookingResponse>() {
            @Override
            public void onResponse(Call<GetBookingResponse> call, Response<GetBookingResponse> response) {
                if (!response.isSuccessful()) {
                    Toast.makeText(getContext(), "Error Code: " + response.code() + response.message(), Toast.LENGTH_LONG).show();
                }
                ArrayList<DataGetBookingResponse> bookingList = response.body().getData();

                for (int i = 0; i < bookingList.size(); i++) {
                    BookingModel booking_model = new BookingModel(
                            bookingList.get(i).getBook_id(),
                            bookingList.get(i).getFloorSectorDesk(),
                            bookingList.get(i).getDateTime(),
                            bookingList.get(i).getStatus());
                    bookingArrayList.add(booking_model);
                }

                recyclerview = view.findViewById(R.id.recyclerview);
                recyclerview.setLayoutManager(new LinearLayoutManager(getContext()));
                recyclerview.setHasFixedSize(true);
                BookingAdapter bookingAdapter = new BookingAdapter(getContext(), bookingArrayList);
                recyclerview.setAdapter(bookingAdapter);
                bookingAdapter.notifyDataSetChanged();
            }

            @Override
            public void onFailure(Call<GetBookingResponse> call, Throwable t) {

            }
        });
    }

//    private void dataInitialize() {
//        bookingArrayList = new ArrayList<>();
//
//        kode_booking = new String[]{
//                "BK20221114174736",
//                "BK20221114174737",
//                "BK20221114174736",
//                "BK20221114174737",
//                "BK20221114174736",
//                "BK20221114174737",
//                "BK20221114174736",
//                "BK20221114174737"
//        };
//
//        floorSectorDesk = new String[]{
//                "1st Floor / A Sector / 1st Desk",
//                "1st Floor / A Sector / 1st Desk",
//                "1st Floor / A Sector / 1st Desk",
//                "1st Floor / A Sector / 1st Desk",
//                "1st Floor / A Sector / 1st Desk",
//                "1st Floor / A Sector / 1st Desk",
//                "1st Floor / A Sector / 1st Desk",
//                "1st Floor / A Sector / 1st Desk"
//        };
//
//        dateTime = new String[]{
//                "2022-10-31, 08:00-16:00",
//                "2022-10-31, 04:00-18:00",
//                "2022-10-31, 08:00-16:00",
//                "2022-10-31, 04:00-18:00",
//                "2022-10-31, 08:00-16:00",
//                "2022-10-31, 04:00-18:00",
//                "2022-10-31, 08:00-16:00",
//                "2022-10-31, 04:00-18:00",
//        };
//
//        statusBooking = new String[]{
//                "BOOKED",
//                "BOOKED",
//                "BOOKED",
//                "BOOKED",
//                "BOOKED",
//                "BOOKED",
//                "BOOKED",
//                "BOOKED"
//        };

//        for (int i = 0; i < kode_booking.length; i++) {
//            BookingModel booking_model = new BookingModel(kode_booking[i], floorSectorDesk[i], dateTime[i], statusBooking[i]);
//            bookingArrayList.add(booking_model);
//        }
//    }
}