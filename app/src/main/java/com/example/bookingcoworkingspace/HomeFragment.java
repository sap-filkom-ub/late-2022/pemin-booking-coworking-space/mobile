package com.example.bookingcoworkingspace;

import static android.content.ContentValues.TAG;

import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.bookingcoworkingspace.Interface.Booking;
import com.example.bookingcoworkingspace.Model.DataGetBookingResponse;
import com.example.bookingcoworkingspace.Model.GetLatestBookingResponse;
import com.example.bookingcoworkingspace.SharedPreference.UserPreferences;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class HomeFragment extends Fragment {
    TextView tvCodeBooking, tvPlace, tvDateTime;
    LinearLayout llStatusTrue, llStatusFalse;

    String BASE_URL = "https://coworking.sparc.id/api/";

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        ViewGroup root = (ViewGroup) inflater.inflate(R.layout.fragment_home, container, false);
        llStatusTrue = root.findViewById(R.id.llStatus1);
        llStatusFalse = root.findViewById(R.id.llStatus0);
        tvCodeBooking = root.findViewById(R.id.booking_number);
        tvPlace = root.findViewById(R.id.booking_place);
        tvDateTime = root.findViewById(R.id.booking_time);

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        Booking booking = retrofit.create(Booking.class);

        Call<GetLatestBookingResponse> call = booking.getLatestBooking(UserPreferences.getUserToken(getContext()));

        call.enqueue(new Callback<GetLatestBookingResponse>() {
            @Override
            public void onResponse(Call<GetLatestBookingResponse> call, Response<GetLatestBookingResponse> response) {
                if (response.body() == null) {
                    Log.d(TAG, "onResponse: " + response.body());
                    llStatusFalse.setVisibility(View.VISIBLE);
                    llStatusTrue.setVisibility(View.GONE);
                } else {
                    DataGetBookingResponse bookingResponse = response.body().getData();
                    Log.d(TAG, "onResponse: " + bookingResponse);
                    llStatusFalse.setVisibility(View.GONE);
                    llStatusTrue.setVisibility(View.VISIBLE);
                    tvCodeBooking.setText(bookingResponse.getBook_id());
                    tvPlace.setText(bookingResponse.getFloorSectorDesk());
                    tvDateTime.setText(bookingResponse.getDateTime());
                }
            }

            @Override
            public void onFailure(Call<GetLatestBookingResponse> call, Throwable t) {
            }
        });

        return root;
    }
}