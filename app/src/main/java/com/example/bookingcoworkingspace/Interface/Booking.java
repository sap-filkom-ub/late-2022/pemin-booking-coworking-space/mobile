package com.example.bookingcoworkingspace.Interface;

import com.example.bookingcoworkingspace.Model.GetBookingResponse;
import com.example.bookingcoworkingspace.Model.GetLatestBookingResponse;
import com.example.bookingcoworkingspace.Model.PostBookingResponse;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Header;

public interface Booking {
    @GET("list")
    Call<GetBookingResponse> getBookings(@Header("Authorization") String userToken);

    @GET("booking/latest")
    Call<GetLatestBookingResponse> getLatestBooking(@Header("Authorization") String userToken);
}
