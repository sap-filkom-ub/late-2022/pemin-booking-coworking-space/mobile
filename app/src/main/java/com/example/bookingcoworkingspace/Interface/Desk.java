package com.example.bookingcoworkingspace.Interface;

import com.example.bookingcoworkingspace.Model.PostBookingResponse;
import com.example.bookingcoworkingspace.Model.DeskResponse;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface Desk {
    @GET("list")
    Call<DeskResponse> getDesk(@Header("Authorization") String userToken,
                               @Query("date") String date,
                               @Query("start_time") String start_time,
                               @Query("end_time") String end_time);

    @FormUrlEncoded
    @POST("booking")
    Call<PostBookingResponse> postBooking(@Header("Authorization") String userToken,
                                          @Field("user_id") String user_id,
                                          @Field("desk_id") String desk_id,
                                          @Field("date") String date,
                                          @Field("start_time") String start_time,
                                          @Field("end_time") String end_time);
}
