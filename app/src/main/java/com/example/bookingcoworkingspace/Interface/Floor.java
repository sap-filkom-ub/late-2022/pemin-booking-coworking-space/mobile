package com.example.bookingcoworkingspace.Interface;

import com.example.bookingcoworkingspace.Model.FloorResponse;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Headers;

public interface Floor {
//    @Headers("Authorization: Bearer 60|ia8D7tlvcvUhcMts0yDI8v7cQgq8k2EBKR6qws2B")
    @GET("floor/list")
    Call<FloorResponse> getFloors(@Header("Authorization") String userToken);
//            @Field("id") Integer id,
//            @Field("name") String name,
//            @Field("description") String description
//    );
}
