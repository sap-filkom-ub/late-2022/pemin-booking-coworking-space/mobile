package com.example.bookingcoworkingspace.Interface;

import com.example.bookingcoworkingspace.Model.LogoutResponse;

import retrofit2.Call;
import retrofit2.http.Header;
import retrofit2.http.POST;

public interface Logout {
    @POST("logout")
    Call<LogoutResponse> logout(@Header("Authorization") String userToken);
}
