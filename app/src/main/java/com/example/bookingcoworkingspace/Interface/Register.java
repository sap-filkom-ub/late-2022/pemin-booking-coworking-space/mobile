package com.example.bookingcoworkingspace.Interface;

import com.example.bookingcoworkingspace.Model.LoginResponse;
import com.example.bookingcoworkingspace.Model.RegisterResponse;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

public interface Register {
        @POST("register")
        @FormUrlEncoded
        Call<RegisterResponse> register(
                @Field("name") String name,
                @Field("username") String username,
                @Field("email") String email,
                @Field("password") String password,
                @Field("password_confirmation") String password_confirmation
        );

}
