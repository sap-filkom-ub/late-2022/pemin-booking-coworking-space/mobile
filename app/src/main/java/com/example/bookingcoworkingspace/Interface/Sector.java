package com.example.bookingcoworkingspace.Interface;

import com.example.bookingcoworkingspace.Model.FloorResponse;
import com.example.bookingcoworkingspace.Model.SectorResponse;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Headers;

public interface Sector {
//    @Headers("Authorization: Bearer 60|ia8D7tlvcvUhcMts0yDI8v7cQgq8k2EBKR6qws2B")
    @GET("list")
    Call<SectorResponse> getSector(@Header("Authorization") String userToken);
}
