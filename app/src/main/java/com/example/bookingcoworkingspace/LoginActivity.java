package com.example.bookingcoworkingspace;

import static android.content.ContentValues.TAG;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.bookingcoworkingspace.Interface.Login;
import com.example.bookingcoworkingspace.Model.DataLoginResponse;
import com.example.bookingcoworkingspace.Model.LoginRequest;
import com.example.bookingcoworkingspace.Model.LoginResponse;
import com.example.bookingcoworkingspace.SharedPreference.UserPreferences;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener {
    TextView register;
    EditText etUsername, etPassword;
    Button btnLogin;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        etUsername = (EditText) findViewById(R.id.username);
        etPassword = (EditText) findViewById(R.id.password);
        register = (TextView) findViewById(R.id.tv_register);
        btnLogin = (Button) findViewById(R.id.btnLogin);

        register.setOnClickListener(this);
        btnLogin.setOnClickListener(this);

    }

    @Override
    public void onClick(View view) {
        if (view.getId() == register.getId()) {
            Intent intent = new Intent(LoginActivity.this, RegisterActivity.class);
            startActivity(intent);
        }
        if (view.getId() == btnLogin.getId()) {
            btnLogin.setEnabled(false);

            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl("https://coworking.sparc.id/api/")
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();

            Login login = retrofit.create(Login.class);

            LoginRequest loginRequest = new LoginRequest(
                    etUsername.getText().toString(), etPassword.getText().toString());

            Call<LoginResponse> call = login.login(loginRequest.getUsername(), loginRequest.getPassword());

            call.enqueue(new Callback<LoginResponse>() {
                @Override
                public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {
                    btnLogin.setEnabled(true);

                    if (!response.isSuccessful()) {
                        Toast.makeText(getApplicationContext(), "Error Code: " + response.code() + "\nCredential Doesn't Match", Toast.LENGTH_LONG).show();
                    } else {
                        UserPreferences.setUserId(getBaseContext(), response.body().getData().getId());
                        UserPreferences.setUserName(getBaseContext(), response.body().getData().getName());
                        UserPreferences.setUserToken(getBaseContext(), "Bearer " + response.body().getData().getToken());
                        DataLoginResponse user = response.body().getData();
                        Log.d(TAG, "onResponse: \n" +
                                "id: " + user.getId() + "\n" +
                                "name: " + user.getName() + "\n" +
                                "token: " + user.getToken() + "\n\n");
                        Log.d(TAG, "onResponse: UserPreferences: \n" +
                                "id: " + UserPreferences.getUserId(getBaseContext()) + "\n" +
                                "name: " + UserPreferences.getUserName(getBaseContext()) + "\n" +
                                "token: " + UserPreferences.getUserToken(getBaseContext()) + "\n\n");

                        Toast.makeText(getApplicationContext(), response.body().getMessage(), Toast.LENGTH_LONG).show();

                        Intent intent = new Intent(LoginActivity.this, NavigationActivity.class);
                        startActivity(intent);
                    }
                }

                @Override
                public void onFailure(Call<LoginResponse> call, Throwable t) {
                    btnLogin.setEnabled(true);
                    Toast.makeText(getApplicationContext(), t.getMessage(), Toast.LENGTH_LONG).show();
                }
            });
        }
    }
}