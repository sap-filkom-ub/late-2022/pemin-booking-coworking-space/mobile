package com.example.bookingcoworkingspace.Model;

public class BookingModel {
    public String kodeBooking, floorSectorDesk, dateTime, statusBooking;

    public BookingModel(String kodeBooking, String floorSectorDesk, String dateTime, String statusBooking) {
        this.kodeBooking = kodeBooking;
        this.floorSectorDesk = floorSectorDesk;
        this.dateTime = dateTime;
        this.statusBooking = statusBooking;
    }
}
