package com.example.bookingcoworkingspace.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.sql.Time;

public class DataGetBookingResponse {

    @SerializedName("id")
    @Expose
    private String id;

    @SerializedName("book_id")
    @Expose
    private String book_id;

//    @SerializedName("user_id")
//    @Expose
//    private String user_id;

//    @SerializedName("desk_id")
//    @Expose
//    private String desk_id;

//    @SerializedName("date")
//    @Expose
//    private String date;
//
//    @SerializedName("start_time")
//    @Expose
//    private String start_time;
//
//    @SerializedName("end_time")
//    @Expose
//    private String end_time;

    @SerializedName("status")
    @Expose
    private String status;

    @SerializedName("floor_sector_desk")
    @Expose
    private String floor_sector_desk;

    @SerializedName("date_time")
    @Expose
    private String date_time;

//    @SerializedName("time")
//    @Expose
//    private TimeDataBooking time;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getBook_id() {
        return book_id;
    }

    public void setBook_id(String book_id) {
        this.book_id = book_id;
    }

//    public String getUser_id() {
//        return user_id;
//    }
//
//    public void setUser_id(String user_id) {
//        this.user_id = user_id;
//    }

//    public String getDesk_id() {
//        return desk_id;
//    }
//
//    public void setDesk_id(String desk_id) {
//        this.desk_id = desk_id;
//    }

//    public String getDate() {
//        return date;
//    }
//
//    public void setDate(String date) {
//        this.date = date;
//    }
//
//    public String getStart_time() {
//        return start_time;
//    }
//
//    public void setStart_time(String start_time) {
//        this.start_time = start_time;
//    }
//
//    public String getEnd_time() {
//        return end_time;
//    }
//
//    public void setEnd_time(String end_time) {
//        this.end_time = end_time;
//    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getFloorSectorDesk() {
        return floor_sector_desk;
    }

    public void setFloorSectorDesk(String floor_sector_desk) {
        this.floor_sector_desk = floor_sector_desk;
    }

    public String getDateTime() {
        return date_time;
    }

    public void setDateTime(String date_time) {
        this.date_time = date_time;
    }

//    public TimeDataBooking getTime() {
//        return time;
//    }
//
//    public void setTime(TimeDataBooking time) {
//        this.time = time;
//    }

    @Override
    public String toString() {
        return "DataGetBookingResponse{" +
                "id='" + id + '\'' +
                ", book_id='" + book_id + '\'' +
//                ", user_id='" + user_id + '\'' +
//                ", desk_id='" + desk_id + '\'' +
//                ", date='" + date + '\'' +
//                ", start_time='" + start_time + '\'' +
//                ", end_time='" + end_time + '\'' +
                ", status='" + status + '\'' +
                ", floor_sector_desk='" + floor_sector_desk + '\'' +
                ", date_time='" + date_time + '\'' +
//                ", time=" + time +
                '}';
    }
}
