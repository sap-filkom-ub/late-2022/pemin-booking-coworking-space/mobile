package com.example.bookingcoworkingspace.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class FloorResponse {

    @SerializedName("success")
    @Expose
    private Boolean success;

    @SerializedName("message")
    @Expose
    private String message;

    @SerializedName("data")
    @Expose
    private ArrayList<DataFloorResponse> data;

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public ArrayList<DataFloorResponse> getData() {
        return data;
    }

    public void setData(ArrayList<DataFloorResponse> data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return "FloorResponse{" +
                "success=" + success +
                ", message='" + message + '\'' +
                ", data=" + data +
                '}';
    }

    //    @SerializedName("data")
//    public List<M_Floor> floors;
//    public List<M_Floor> data;
}
