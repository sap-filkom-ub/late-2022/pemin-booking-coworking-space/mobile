package com.example.bookingcoworkingspace.Model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class M_Floor {
//    @SerializedName("data")
//    private List<M_Floor> floors;
    @SerializedName("id")
    private Integer id;
    @SerializedName("name")
    private String name;
    @SerializedName("description")
    private String description;

//    public List<M_Floor> getFloors() {
//        return floors;
//    }
//
//    public void setFloors(List<M_Floor> floors) {
//        this.floors = floors;
//    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
