package com.example.bookingcoworkingspace.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PostBookingRequest {

    @SerializedName("user_id")
    @Expose
    private String user_id;

    @SerializedName("desk_id")
    @Expose
    private String desk_id;

    @SerializedName("date")
    @Expose
    private String date;

    @SerializedName("start_time")
    @Expose
    private String start_time;

    @SerializedName("end_time")
    @Expose
    private String end_time;

    public PostBookingRequest(String user_id, String desk_id, String date, String start_time, String end_time) {
        this.user_id = user_id;
        this.desk_id = desk_id;
        this.date = date;
        this.start_time = start_time;
        this.end_time = end_time;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getDesk_id() {
        return desk_id;
    }

    public void setDesk_id(String desk_id) {
        this.desk_id = desk_id;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getStart_time() {
        return start_time;
    }

    public void setStart_time(String start_time) {
        this.start_time = start_time;
    }

    public String getEnd_time() {
        return end_time;
    }

    public void setEnd_time(String end_time) {
        this.end_time = end_time;
    }

    @Override
    public String toString() {
        return "BookingRequest{" +
                "user_id='" + user_id + '\'' +
                ", desk_id='" + desk_id + '\'' +
                ", date='" + date + '\'' +
                ", start_time='" + start_time + '\'' +
                ", end_time='" + end_time + '\'' +
                '}';
    }
}
