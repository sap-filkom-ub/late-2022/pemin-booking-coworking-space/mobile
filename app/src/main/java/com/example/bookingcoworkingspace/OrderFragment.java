package com.example.bookingcoworkingspace;

import static android.content.ContentValues.TAG;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.bookingcoworkingspace.Interface.Floor;
import com.example.bookingcoworkingspace.Model.DataFloorResponse;
import com.example.bookingcoworkingspace.Model.FloorResponse;
import com.example.bookingcoworkingspace.SharedPreference.DataBookingPreferences;
import com.example.bookingcoworkingspace.SharedPreference.UserPreferences;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class OrderFragment extends Fragment {
    LinearLayout firstFloor;
    LinearLayout secondFloor;
    TextView tvFloor1, tvFloor2, tvDesc1, tvDesc2;

    String BASE_URL = "https://coworking.sparc.id/api/";

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_order, container, false);

        firstFloor = view.findViewById(R.id.layout_firstFloor);
        secondFloor = view.findViewById(R.id.layout_secondFloor);
        tvFloor1 = view.findViewById(R.id.floor1);
        tvFloor2 = view.findViewById(R.id.floor2);
        tvDesc1 = view.findViewById(R.id.desc1);
        tvDesc2 = view.findViewById(R.id.desc2);

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        Floor floor = retrofit.create(Floor.class);

        Call<FloorResponse> call = floor.getFloors(UserPreferences.getUserToken(getContext()));
        Log.d(TAG, "onCreateView: Token: " + UserPreferences.getUserToken(getContext()));

        call.enqueue(new Callback<FloorResponse>() {
            @Override
            public void onResponse(Call<FloorResponse> call, Response<FloorResponse> response) {
                if (!response.isSuccessful()) {
                    Toast.makeText(getContext(), "Error Code: " + response.code() + "Cannot Fetch Data", Toast.LENGTH_LONG).show();
                }
                Log.d(TAG, "onResponse: Receive Information: " + response.body().toString());

                ArrayList<DataFloorResponse> floorList = response.body().getData();

                for (int i = 0; i < floorList.size(); i++) {
                    Log.d(TAG, "onResponse: \n" +
                            "Id: " + floorList.get(i).getId() + "\n" +
                            "Name: " + floorList.get(i).getName() + "\n" +
                            "Description: " + floorList.get(i).getDescription() + "\n\n");
                }

                tvFloor1.setText(floorList.get(0).getName());
                tvDesc1.setText(floorList.get(0).getDescription());
                tvFloor2.setText(floorList.get(1).getName());
                tvDesc2.setText(floorList.get(1).getDescription());

                firstFloor.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        DataBookingPreferences.setIdFloor(getContext(), floorList.get(0).getId());
                        DataBookingPreferences.setFloorName(getContext(), floorList.get(0).getName());
                        Fragment sectorFragment = new SectorFragment();
                        FragmentTransaction fm = getActivity().getSupportFragmentManager().beginTransaction();
                        fm.replace(R.id.flFragment, sectorFragment, null).addToBackStack(null).commit();
                    }
                });

                secondFloor.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        DataBookingPreferences.setIdFloor(getContext(), floorList.get(1).getId());
                        DataBookingPreferences.setFloorName(getContext(), floorList.get(1).getName());
                        Fragment sectorFragment = new SectorFragment();
                        FragmentTransaction fm = getActivity().getSupportFragmentManager().beginTransaction();
                        fm.replace(R.id.flFragment, sectorFragment, null).addToBackStack(null).commit();
                    }
                });
            }

            @Override
            public void onFailure(Call<FloorResponse> call, Throwable t) {

            }
        });

        return view;
    }
}