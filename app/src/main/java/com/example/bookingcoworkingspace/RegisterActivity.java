package com.example.bookingcoworkingspace;

import static android.content.ContentValues.TAG;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.bookingcoworkingspace.Interface.Register;
import com.example.bookingcoworkingspace.Model.DataLoginResponse;
import com.example.bookingcoworkingspace.Model.RegisterRequest;
import com.example.bookingcoworkingspace.Model.RegisterResponse;
import com.example.bookingcoworkingspace.SharedPreference.UserPreferences;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RegisterActivity extends AppCompatActivity implements View.OnClickListener {
    Button btnRegister;
    EditText etName, etUsername, etEmail, etPassword, etPasswordConfirmation;
    TextView login;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        etName = (EditText) findViewById(R.id.name_register);
        etUsername = (EditText) findViewById(R.id.username_register);
        etEmail = (EditText) findViewById(R.id.email_register);
        etPassword = (EditText) findViewById(R.id.password_register);
        etPasswordConfirmation = (EditText) findViewById(R.id.confirmPassword_register);
        btnRegister = (Button) findViewById(R.id.btnRegister);
        login = (TextView) findViewById(R.id.tv_login);

        btnRegister.setOnClickListener(this);
        login.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == login.getId()) {
            Intent intent = new Intent(RegisterActivity.this, LoginActivity.class);
            startActivity(intent);
        }
        if (view.getId() == btnRegister.getId()) {
            btnRegister.setEnabled(false);

            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl("https://coworking.sparc.id/api/")
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();

            Register register = retrofit.create(Register.class);

            RegisterRequest registerRequest = new RegisterRequest(
                    etName.getText().toString(),
                    etUsername.getText().toString(),
                    etEmail.getText().toString(),
                    etPassword.getText().toString(),
                    etPasswordConfirmation.getText().toString()
            );

            Call<RegisterResponse> call = register.register(
                    registerRequest.getName(),
                    registerRequest.getUsername(),
                    registerRequest.getEmail(),
                    registerRequest.getPassword(),
                    registerRequest.getPasswordConfirmation()
            );

            call.enqueue(new Callback<RegisterResponse>() {
                @Override
                public void onResponse(Call<RegisterResponse> call, Response<RegisterResponse> response) {
                    btnRegister.setEnabled(true);

                    if (!response.isSuccessful()) {
                        Toast.makeText(getApplicationContext(), "Error Code: " + response.code() + "\n" + response.body().getMessage(), Toast.LENGTH_LONG).show();
                        Log.d(TAG, response.code() + "|" + response.body().getMessage());
                    } else {
                        UserPreferences.setUserId(getBaseContext(), response.body().getData().getId());
                        UserPreferences.setUserName(getBaseContext(), response.body().getData().getName());
                        UserPreferences.setUserToken(getBaseContext(), "Bearer " + response.body().getData().getToken());
                        DataLoginResponse user = response.body().getData();
                        Log.d(TAG, "onResponse: \n" +
                                "id: " + user.getId() + "\n" +
                                "name: " + user.getName() + "\n" +
                                "token: " + user.getToken() + "\n\n");
                        Log.d(TAG, "onResponse: UserPreferences: \n" +
                                "id: " + UserPreferences.getUserId(getBaseContext()) + "\n" +
                                "name: " + UserPreferences.getUserName(getBaseContext()) + "\n" +
                                "token: " + UserPreferences.getUserToken(getBaseContext()) + "\n\n");

                        Toast.makeText(getApplicationContext(), response.body().getMessage(), Toast.LENGTH_LONG).show();

                        Intent intent = new Intent(RegisterActivity.this, NavigationActivity.class);
                        startActivity(intent);
                    }
                }

                @Override
                public void onFailure(Call<RegisterResponse> call, Throwable t) {
                    login.setText("err connection");
                    btnRegister.setEnabled(true);
                }
            });
        }
    }
}