package com.example.bookingcoworkingspace;

import static android.content.ContentValues.TAG;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.bookingcoworkingspace.Interface.Floor;
import com.example.bookingcoworkingspace.Interface.Sector;
import com.example.bookingcoworkingspace.Model.FloorResponse;
import com.example.bookingcoworkingspace.Model.DataSectorResponse;
import com.example.bookingcoworkingspace.Model.SectorResponse;
import com.example.bookingcoworkingspace.SharedPreference.DataBookingPreferences;
import com.example.bookingcoworkingspace.SharedPreference.UserPreferences;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class SectorFragment extends Fragment {
    LinearLayout sectorA, sectorB, sectorC;
    TextView tvSector1, tvSector2, tvSector3, tvDesc1, tvDesc2, tvDesc3;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_sector, container, false);
        sectorA = view.findViewById(R.id.layout_sectorA);
        sectorB = view.findViewById(R.id.layout_sectorB);
        sectorC = view.findViewById(R.id.layout_sectorC);
        tvSector1 = view.findViewById(R.id.sector1);
        tvSector2 = view.findViewById(R.id.sector2);
        tvSector3 = view.findViewById(R.id.sector3);
        tvDesc1 = view.findViewById(R.id.desc1);
        tvDesc2 = view.findViewById(R.id.desc2);
        tvDesc3 = view.findViewById(R.id.desc3);

        String idFloor = DataBookingPreferences.getIdFloor(getContext());
        String BASE_URL = "https://coworking.sparc.id/api/sector/" + idFloor + "/";

        Log.d(TAG, "\n onResponse: FloorIdSelected: " + DataBookingPreferences.getIdFloor(getContext()) + "\n");
        Log.d(TAG, "\n onResponse: BASE_URL: " + BASE_URL + "\n");

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        Sector sector = retrofit.create(Sector.class);
        Call<SectorResponse> call = sector.getSector(UserPreferences.getUserToken(getContext()));

        call.enqueue(new Callback<SectorResponse>() {
            @Override
            public void onResponse(Call<SectorResponse> call, Response<SectorResponse> response) {
                if (!response.isSuccessful()) {
                    Toast.makeText(getContext(), "Error Code: " + response.code() + "Cannot Fetch Data", Toast.LENGTH_LONG).show();
                }
                Log.d(TAG, "onResponse: Receive Information: " + response.body().toString());

                ArrayList<DataSectorResponse> sectorList = response.body().getData();

                for (int i = 0; i < sectorList.size(); i++) {
                    Log.d(TAG, "onResponse: \n" +
                            "id: " + sectorList.get(i).getId() + "\n" +
                            "floor_id: " + sectorList.get(i).getFloor_id() + "\n" +
                            "Name: " + sectorList.get(i).getName() + "\n" +
                            "Description: " + sectorList.get(i).getDescription() + "\n\n");
                }

                tvSector1.setText(sectorList.get(0).getName());
                tvSector2.setText(sectorList.get(1).getName());
                tvSector3.setText(sectorList.get(2).getName());
                tvDesc1.setText(sectorList.get(0).getDescription());
                tvDesc2.setText(sectorList.get(1).getDescription());
                tvDesc3.setText(sectorList.get(2).getDescription());

                sectorA.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        DataBookingPreferences.setIdSector(getContext(), sectorList.get(0).getId());
                        DataBookingPreferences.setSectorName(getContext(), sectorList.get(0).getName());
                        Fragment deskFragment = new DeskFragment();
                        FragmentTransaction fm = getActivity().getSupportFragmentManager().beginTransaction();
                        fm.replace(R.id.flFragment, deskFragment, null).addToBackStack(null).commit();
                    }
                });

                sectorB.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        DataBookingPreferences.setIdSector(getContext(), sectorList.get(1).getId());
                        DataBookingPreferences.setSectorName(getContext(), sectorList.get(1).getName());
                        Fragment deskFragment = new DeskFragment();
                        FragmentTransaction fm = getActivity().getSupportFragmentManager().beginTransaction();
                        fm.replace(R.id.flFragment, deskFragment, null).addToBackStack(null).commit();
                    }
                });

                sectorC.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        DataBookingPreferences.setIdSector(getContext(), sectorList.get(2).getId());
                        DataBookingPreferences.setSectorName(getContext(), sectorList.get(2).getName());
                        Fragment deskFragment = new DeskFragment();
                        FragmentTransaction fm = getActivity().getSupportFragmentManager().beginTransaction();
                        fm.replace(R.id.flFragment, deskFragment, null).addToBackStack(null).commit();
                    }
                });
            }

            @Override
            public void onFailure(Call<SectorResponse> call, Throwable t) {

            }
        });

        return view;
    }
}