package com.example.bookingcoworkingspace;

import static android.content.ContentValues.TAG;

import android.content.Intent;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.bookingcoworkingspace.Interface.Login;
import com.example.bookingcoworkingspace.Interface.Logout;
import com.example.bookingcoworkingspace.Model.DataLoginResponse;
import com.example.bookingcoworkingspace.Model.FloorResponse;
import com.example.bookingcoworkingspace.Model.LogoutResponse;
import com.example.bookingcoworkingspace.SharedPreference.DataBookingPreferences;
import com.example.bookingcoworkingspace.SharedPreference.UserPreferences;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class SettingFragment extends Fragment {
    ImageView ivLogout;
    TextView tvUsername;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_setting, container, false);
        tvUsername = view.findViewById(R.id.user_name);
        tvUsername.setText(UserPreferences.getUserName(getContext()));

        ivLogout = view.findViewById(R.id.logout);

        ivLogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Retrofit retrofit = new Retrofit.Builder()
                        .baseUrl("https://coworking.sparc.id/api/")
                        .addConverterFactory(GsonConverterFactory.create())
                        .build();

                Logout logout = retrofit.create(Logout.class);

                Call<LogoutResponse> call = logout.logout(UserPreferences.getUserToken(getContext()));
                Log.d(TAG, "onCreateView: Token: " + UserPreferences.getUserToken(getContext()));

                call.enqueue(
                        new Callback<LogoutResponse>() {
                            @Override
                            public void onResponse(Call<LogoutResponse> call, Response<LogoutResponse> response) {
                                if (!response.isSuccessful()) {
                                    Toast.makeText(getContext(), "Error Code: " + response.code() + "\n" + response.body().getMessage(), Toast.LENGTH_LONG).show();
                                    Log.d(TAG, response.body().getMessage());
                                } else {
                                    UserPreferences.setUserId(getContext(), "");
                                    UserPreferences.setUserName(getContext(), "");
                                    UserPreferences.setUserToken(getContext(), "");

                                    Toast.makeText(getContext(), response.body().getMessage(), Toast.LENGTH_LONG).show();

                                    getActivity().finish();
                                }
                            }

                            @Override
                            public void onFailure(Call<LogoutResponse> call, Throwable t) {
                                Toast.makeText(getContext(), t.getMessage(), Toast.LENGTH_LONG).show();
                                Log.d(TAG, t.getMessage());
                            }
                        }
                );

            }
        });

        return view;
    }
}