package com.example.bookingcoworkingspace.SharedPreference;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

public class DataBookingPreferences {
    static final String KEY_ID_FLOOR = "1";
    static final String KEY_FLOOR_NAME = "1st Floor";
    static final String KEY_ID_SECTOR = "2";
    static final String KEY_SECTOR_NAME = "B Sector";
    static final String KEY_ID_DESK = "3";
    static final String KEY_DESK_NAME = "3rd Desk";

    private static SharedPreferences getSharedPreference(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context);
    }

    public static void setIdFloor(Context context, String idFloor) {
        SharedPreferences.Editor editor = getSharedPreference(context).edit();
        editor.putString(KEY_ID_FLOOR, idFloor);
        editor.apply();
    }

    public static String getIdFloor(Context context) {
        return getSharedPreference(context).getString(KEY_ID_FLOOR, "");
    }

    public static void setFloorName(Context context, String floorName) {
        SharedPreferences.Editor editor = getSharedPreference(context).edit();
        editor.putString(KEY_FLOOR_NAME, floorName);
        editor.apply();
    }

    public static String getFloorName(Context context) {
        return getSharedPreference(context).getString(KEY_FLOOR_NAME, "");
    }

    public static void setIdSector(Context context, String idSector) {
        SharedPreferences.Editor editor = getSharedPreference(context).edit();
        editor.putString(KEY_ID_SECTOR, idSector);
        editor.apply();
    }

    public static String getIdSector(Context context) {
        return getSharedPreference(context).getString(KEY_ID_SECTOR, "");
    }

    public static void setSectorName(Context context, String sectorName) {
        SharedPreferences.Editor editor = getSharedPreference(context).edit();
        editor.putString(KEY_SECTOR_NAME, sectorName);
        editor.apply();
    }

    public static String getSectorName(Context context) {
        return getSharedPreference(context).getString(KEY_SECTOR_NAME, "");
    }

    public static void setIdDesk(Context context, String idDesk) {
        SharedPreferences.Editor editor = getSharedPreference(context).edit();
        editor.putString(KEY_ID_DESK, idDesk);
        editor.apply();
    }

    public static String getIdDesk(Context context) {
        return getSharedPreference(context).getString(KEY_ID_DESK, "");
    }

    public static void setDeskName(Context context, String deskName) {
        SharedPreferences.Editor editor = getSharedPreference(context).edit();
        editor.putString(KEY_DESK_NAME, deskName);
        editor.apply();
    }

    public static String getDeskName(Context context) {
        return getSharedPreference(context).getString(KEY_DESK_NAME, "");
    }
}
