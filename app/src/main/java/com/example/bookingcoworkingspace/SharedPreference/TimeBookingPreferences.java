package com.example.bookingcoworkingspace.SharedPreference;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

public class TimeBookingPreferences {
    static final String KEY_DATE_SELECTED = "2022-01-02";
    static final String KEY_START_TIME = "10:10";
    static final String KEY_END_TIME = "20:20";

    private static SharedPreferences getSharedPreference(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context);
    }

    public static void setDateSelected(Context context, String dataSelected) {
        SharedPreferences.Editor editor = getSharedPreference(context).edit();
        editor.putString(KEY_DATE_SELECTED, dataSelected);
        editor.apply();
    }

    public static String getDateSelected(Context context) {
        return getSharedPreference(context).getString(KEY_DATE_SELECTED, "");
    }

    public static void setStartTime(Context context, String startTime) {
        SharedPreferences.Editor editor = getSharedPreference(context).edit();
        editor.putString(KEY_START_TIME, startTime);
        editor.apply();
    }

    public static String getStartTime(Context context) {
        return getSharedPreference(context).getString(KEY_START_TIME, "");
    }

    public static void setEndTime(Context context, String endTime) {
        SharedPreferences.Editor editor = getSharedPreference(context).edit();
        editor.putString(KEY_END_TIME, endTime);
        editor.apply();
    }

    public static String getEndTime(Context context) {
        return getSharedPreference(context).getString(KEY_END_TIME, "");
    }
}
