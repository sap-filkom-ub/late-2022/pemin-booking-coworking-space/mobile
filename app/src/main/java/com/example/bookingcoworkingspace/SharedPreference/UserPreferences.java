package com.example.bookingcoworkingspace.SharedPreference;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

public class UserPreferences {
    static final String KEY_USER_ID = "User_Id";
    static final String KEY_USER_NAME = "User_Name";
    static final String KEY_USER_TOKEN = "User_Token";

    private static SharedPreferences getSharedPreference(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context);
    }

    public static void setUserId(Context context, String idUser) {
        SharedPreferences.Editor editor = getSharedPreference(context).edit();
        editor.putString(KEY_USER_ID, idUser);
        editor.apply();
    }

    public static String getUserId(Context context) {
        return getSharedPreference(context).getString(KEY_USER_ID, "");
    }

    public static void setUserName(Context context, String nameUser) {
        SharedPreferences.Editor editor = getSharedPreference(context).edit();
        editor.putString(KEY_USER_NAME, nameUser);
        editor.apply();
    }

    public static String getUserName(Context context) {
        return getSharedPreference(context).getString(KEY_USER_NAME, "");
    }

    public static void setUserToken(Context context, String tokenUser) {
        SharedPreferences.Editor editor = getSharedPreference(context).edit();
        editor.putString(KEY_USER_TOKEN, tokenUser);
        editor.apply();
    }

    public static String getUserToken(Context context) {
        return getSharedPreference(context).getString(KEY_USER_TOKEN, "");
    }
}
